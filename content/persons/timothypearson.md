---
title: "Timothy Pearson"
image: "timothypearson-200.jpg"
member: raptorcomputingsystems
date: 2021-03-03
draft: false
---

Timothy has worked in multiple areas of high performance systems design over the past 15 years,
from hardware / firmware to kernel and application development, with a strong emphasis on open solutions and system security.
In his time at Raptor he has been heavily involved in the inception and creation of the Talos and Blackbird lines of secure,
owner-controlled OpenPOWER machines, along with the FlexVer remote attestation technology.
